from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from apitard import views

urlpatterns = [
    path('tard', views.ApiTardList.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
